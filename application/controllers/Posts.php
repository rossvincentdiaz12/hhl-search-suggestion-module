<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

  public function __construct(){
 
      parent::__construct();
      $this->load->helper('url');
      // Load session
      $this->load->library('session');
      // Load model 
      $this->load->model('Main_model');

    }
//controller for search_view
    public function index(){

        // Load view
        $data['posts'] = $this->Main_model->getProduct();
        $this->load->view('search_view',$data);

    }   
//controller for item_view to display search item
public function itemview(){
    
 $data['posts'] = $this->Main_model->getSearchProduct($_GET['search_box']);
 $this->load->view('item_view',$data);
 

 }     
//controller for saving customer ip and search
public function savecustomer(){
    //print_r($_POST);
    $result = $this->Main_model->submit();
   
    if($result){
        $this->session->set_flashdata('success_msg', ' comment added successfully');
   }else{
        $this->session->set_flashdata('error_msg', 'fail to add comment');
   }
   
   redirect(base_url('posts/itemview?search_box=' . $_POST['search_box']));
   
}
//controller for Search function with typeahead suggestions
    function fetch(){

    $this->load->model('main_model');
    echo $this->main_model->fetch_data($this->uri->segment(3));
    
    }
//controller for Live Search function to display product
    function fetch_live()
    {
     $output = '';
     $query = '';
     $this->load->model('main_model');
     if($this->input->post('query'))
     {
      $query = $this->input->post('query');
     }
     $data = $this->main_model->fetch_data_live($query);
     $output .= '
     <div class="table-responsive">
        <table class="table table-bordered table-striped">
         <tr>
          <th>Product Name</th>
          <th>Product Image</th>
    
         </tr>
     ';
     if($data->num_rows() > 0)
     {
      foreach($data->result() as $row)
      {
       $output .= '
         <tr>
          <td>'.$row->ProductName.'</td>
      	<td><img src="image/'.$row->ProductImage.'" width="120" ></td>
         </tr>
       ';
      }
     }
     else
     {
      $output .= '<tr>
          <td colspan="5">No Data Found</td>
         </tr>';
     }
     $output .= '</table>';
     echo $output;
    }
        
}