<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Main_model extends CI_Model {

    public function __construct() {
		parent::__construct(); 
	}
//displaying products ascending
	public function getProduct(){

		$this->db->order_by('ProductID', 'asc');
		$query = $this->db->get('tbl_products');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
 
	}
//displaying search
//update is the search value
public function getSearchProduct($search){
	
	$this->db->select('*');
	$this->db->from('tbl_products');
	$this->db->like('ProductName', $search);
	$query = $this->db->get();
	if($query->num_rows() > 0){
		return $query->result();
	}else{
		return false;
	}
	
}
//submitting search record of the customer via ip
	public function submit(){

	  $field = array(
		  'CustomerIP'=>$this->input->post('ip_text'),
		  'CustomerSearch'=>$this->input->post('search_box'),
		  'Date'=>$this->input->post('date_text'),
		  );
	  $this->db->insert('tbl_customer', $field);
	  if($this->db->affected_rows() > 0){
		  return true;
	   
	  }else{ 
		  return false;
	  }
			  
	}
//submitting search record of the customer via ip
	function fetch_data($query){

	 $this->db->like('ProductName', $query);
	 $query = $this->db->get('tbl_products');
	 if($query->num_rows() > 0){
	  foreach($query->result_array() as $row){
	   $output[] = array(
		'name'  => $row["ProductName"],
		'image'  => $row["ProductImage"]
	   );
	  }
	  echo json_encode($output);
	 }

	}

//live query to display product closes to what customer search
	function fetch_data_live($query)
	{
	 $this->db->select("*");
	 $this->db->from("tbl_products");
	
	  $this->db->like('ProductName', $query);
   
	
	 return $this->db->get();
	}

}