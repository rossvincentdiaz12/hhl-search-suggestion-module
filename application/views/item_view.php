<html>
<head>
    <title>Holistic Health Labs</title>
    
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://twitter.github.io/typeahead.js/css/examples.css" /> 

</head>
<body>
 <div class="container">
  <br/><br/><br/>
  <!--Search function with typeahead suggestions-->
  <div > <a href="<?php echo base_url('/'); ?>" class="btn btn-success">Back</a></div>
  <br/><br/><br/>
  <!--displaying products ascending-->
  <div>
		<table class="table table-bordered table-responsive">
			<thead>
				<tr>			
					<th>Product</th>
					<th>image</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if($posts){
					foreach($posts as $post){
					?>
				<tr>					
					<td><?php echo $post->ProductName; ?></td>
					<td><img src="../image/<?php echo $post->ProductImage; ?>" width="120" ></td>
				</tr>
			<?php
					}
				}
			?>
      </tbody>
		</table>
	</div>
 </div>


</body>
</html>


