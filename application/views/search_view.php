<!--getting Ip address of the customers computer-->
<?php  
$ip_address =gethostbyname ("www.holistichealthlabs.com");  
echo "Your IP Address in Holistic Health Labs is - ".$ip_address;  
?>  
<html>
<head> 
    <title>Holistic Health Labs</title>
    
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://twitter.github.io/typeahead.js/css/examples.css" /> 
 <!--Plugins for typeahead functions-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
 <script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
 <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

</head>
<body>
 <div class="container">
  <br/><br/><br/>
  <!--Search function with typeahead suggestions-->
  <div id="prefetch">
  <input type="text" name="search_box" id="search_box" class="form-control input-lg typeahead" placeholder="Search Here" />
  	<form action="<?php echo base_url('posts/savecustomer')?>" method="post" class="form-horizontal">
  	
			<input type= "hidden" Value ="<?php echo $ip_address; ?>" name="ip_text" id="ip_text"/>
			<input type='hidden' id='date_text' name="date_text" value='<?php echo date('Y-m-d');?>'>
			<input type="submit" name="btnSave" class="btn btn-primary" value="Search">
		</form>
  </div>
  <br/><br/><br/>
  <!--displaying products ascending-->
  <div id="result"></div>
</body>
</html>

<!--Typeahead and Live Search script-->

<script>
$(document).ready(function(){

//loading methodto display on div result
  load_data();
//posting data to div result
 function load_data(query)
 {
  $.ajax({
   url:"<?php echo base_url(); ?>posts/fetch_live",
   method:"POST",
   data:{query:query},
   success:function(data){
    $('#result').html(data);
   }
  })
 }
//cheking the value of the search_box to load the data live every key
 $('#search_box').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });

});
</script>

