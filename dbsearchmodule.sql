/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.7.24 : Database - dbsearchmodule
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbsearchmodule` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbsearchmodule`;

/*Table structure for table `tbl_customer` */

DROP TABLE IF EXISTS `tbl_customer`;

CREATE TABLE `tbl_customer` (
  `SearchID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerIP` varchar(250) NOT NULL,
  `CustomerSearch` varchar(250) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`SearchID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_customer` */

insert  into `tbl_customer`(`SearchID`,`CustomerIP`,`CustomerSearch`,`Date`) values (1,'69.16.209.112','ss','2020-05-28'),(2,'69.16.209.112','ss','2020-05-28'),(3,'69.16.209.112','aa','2020-05-28'),(4,'69.16.209.112','sss','2020-05-28'),(5,'69.16.209.112','fff','2020-05-28'),(6,'69.16.209.112','fff','2020-05-28'),(7,'69.16.209.112','sample','2020-05-28'),(8,'69.16.209.112','sample','2020-05-28'),(9,'69.16.209.112','New Vision Alive','2020-05-28'),(10,'69.16.209.112','New Vision Alive','2020-05-28'),(11,'69.16.209.112','New Vision Alive','2020-05-28'),(12,'69.16.209.112','sample','2020-05-29'),(13,'69.16.209.112','c','2020-05-29'),(14,'69.16.209.112','2','2020-05-29'),(15,'69.16.209.112','new','2020-05-29'),(16,'69.16.209.112','New Vision Alive','2020-05-29'),(17,'69.16.209.112','','2020-05-29'),(18,'69.16.209.112','New Vision Alive','2020-05-29'),(19,'69.16.209.112','Gut Alive','2020-05-29'),(20,'69.16.209.112','New Vision Alive','2020-05-29'),(21,'69.16.209.112','New Vision Alive','2020-05-29'),(22,'69.16.209.112','New Vision Alive','2020-05-29'),(23,'69.16.209.112','Myco Ultra','2020-05-29'),(24,'69.16.209.112','s','2020-05-29');

/*Table structure for table `tbl_products` */

DROP TABLE IF EXISTS `tbl_products`;

CREATE TABLE `tbl_products` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(250) NOT NULL,
  `ProductImage` varchar(2500) NOT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_products` */

insert  into `tbl_products`(`ProductID`,`ProductName`,`ProductImage`) values (1,'New Vision Alive','New Vision Alive.png'),(2,'Gut Alive','Gut Alive.jpg'),(3,'Uri Alive','Uri Alive.png'),(4,'Myco Ultra','Myco Ultra.png'),(5,'Energize Greens','Energize Greens.png'),(6,'Vitamin K2 And D3 With BioPerine','Vitamin K2 And D3 With BioPerine.png'),(7,'Organic Elderberry Shield','Organic Elderberry Shield.png');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
